FROM b4k3r/cs16-server

COPY config/server.cfg config/motd.txt config/mapcycle.txt /home/steam/cs16/cstrike/
COPY config/podbot.cfg config/botnames.txt /home/steam/cs16/cstrike/addons/podbot/

WORKDIR /home/steam/cs16

EXPOSE 27015/tcp 27015/udp

ENTRYPOINT ['./hlds_run', '-game', 'cstrike', '-strictportbind', '-autoupdate', '-ip', '0.0.0.0', '+sv_lan', '1', '+map', 'de_dust2', '-maxplayers', '32']
